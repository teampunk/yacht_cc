
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from main.views import HomeView,YatchDetailView, Summary,get_product_details, pdf,ContactView
from main import views as main_views
from django.views.decorators.http import require_http_methods



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', HomeView.as_view(), name = "home" ),
    url(r'^yacht/(?P<pk>[\d]+)', YatchDetailView.as_view(), name="yatch_detail"),
    url(r'^summary/(?P<pk>[\d]+)', main_views.Summary, name="summary"),
    url(r'^update_summary/', main_views.update_summary, name = "update_summary" ),
    url(r'^get_slides_360/', main_views.get_slides_360, name = "get_slides_360" ),
    url(r'^get_product_details/', main_views.get_product_details, name = "get_product_details" ),    
    url(r'^get_color_details/', main_views.get_color_details, name = "get_color_details" ),    
    url(r'^generate_code/', main_views.generate_code, name = "generate_code" ),
    url(r'^generate_pdf/', main_views.pdf, name="generate_pdf"),
    url(r'^summary_code/', main_views.summary_code, name='summary_code'),
    url(r'^code/(?P<code>[\w\-]+)$', main_views.code, name='code'),
    url(r'^code/', main_views.blankcode, name='blankcode'),
    url(r'^contactanos', main_views.contacto, name='contacto'),
    url(r'^save-form', main_views.ContactView.as_view(), name='formview'),
    # url(r'^contacto/$', main_views.Contact.as_view(), name='contact'),
    # url(r'^contacto/recibido/$', main_views.contact_success, name='contact-success'),
    # url('^my_form/$', require_POST(main_views.as_view()), name='form-contact'),






] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
