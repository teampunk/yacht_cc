from django.contrib import admin
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
from .models import *
from sorl.thumbnail.admin import AdminImageMixin


# class SlideInline(admin.TabularInline):
#     model = Slide
#     extra = 2

# class SlideShowAdmin(admin.ModelAdmin):
# 	filter_horizontal = ('color', )
# 	raw_id_fields= ('Yatch', )
# 	inlines = [SlideInline, ]


# class ColorAdmin(admin.ModelAdmin):
# 	list_display = ('name', 'color_category' )


# class ColorCategoryAdmin(admin.ModelAdmin):
# 	list_display = ('name', 'color_type_category')

# class YatchCharacteristicAdmin(admin.ModelAdmin):
# 	list_display = ('name', 'characteristic_category' )
# 	raw_id_fields = ('characteristic_category',)


class SlideTabInline(admin.TabularInline):
    model = Slide
    extra = 2


class SlideInline(NestedStackedInline):
    model = Slide
    extra = 4


class ItemInline(NestedStackedInline):
    model = Item
    extra = 4


class SlideProductInline(NestedStackedInline):
    model = ProductSlide
    extra = 4


class ItemTabInline(NestedStackedInline):
    model = Item
    extra = 8


class ThreesixtySliderInline(AdminImageMixin, NestedStackedInline):
    model = ThreesixtySlider
    extra = 1
    filter_horizontal = ('color',)
    inlines = [ItemInline]


class SlideShowInline(AdminImageMixin, NestedStackedInline):
    model = SlideShow
    extra = 1
    filter_horizontal = ('color',)

    inlines = [SlideInline]


@admin.register(CustomConfig)
class CustomConfigAdmin(admin.ModelAdmin):
    list_display = ('code', 'date_created')
    list_filter = ('date_created',)
    search_fields = ('code',)


class YatchCharacteristicInline(NestedStackedInline):
    model = YatchCharacteristic
    extra = 1


class YatchAdmin(NestedModelAdmin):
    list_display = ('name', 'imagen_yate',)
    search_fields = ('name',)
    list_editable = ('name',)
    filter_horizontal = ('product', 'color')

    inlines = [YatchCharacteristicInline]

    def imagen_yate(self, obj):
        return '<img src="%s">' % obj.thumbnail.url

    imagen_yate.allow_tags = True


class ProductAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ('name', 'category', )
    list_filter = (
        ('category', admin.RelatedOnlyFieldListFilter),

    )
    inline = [SlideProductInline, ]


class ColorAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ('id', 'name', 'color_category', 'type',)
    search_fields = ('name',)
    list_filter = (
        ('color_category', admin.RelatedOnlyFieldListFilter),

    )


class ColorCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'color_type_category',)
    search_fields = ('name',)
    list_filter = (
        ('color_type_category', admin.RelatedOnlyFieldListFilter),
    )


class SlideShowAdmin(admin.ModelAdmin):
    list_display = ('name', 'vandutch')
    search_fields = ('name',)
    filter_horizontal = ('color',)
    inlines = [SlideTabInline, ]
    list_filter = (
        ('Yatch', admin.RelatedOnlyFieldListFilter),
    )

# class ThreeSixtySlideAdmin(admin.ModelAdmin):
# 	list_display = ('name', 'vandutch')
# 	search_fields = ('name',)
# 	list_filter = (
#         ('yatch', admin.RelatedOnlyFieldListFilter),
#     )


class SlideShowTabAdmin(AdminImageMixin, admin.ModelAdmin):
    filter_horizontal = ('color',)
    search_fields = ('name',)

    inlines = [SlideTabInline, ]


class ItemShowTabAdmin(admin.ModelAdmin):
    filter_horizontal = ('color',)
    search_fields = ('name',)

    inlines = [ItemTabInline, ]


class YatchCharacteristicCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'type_characteristic_category')
    search_fields = ('name',)
    list_filter = (
        ('type_characteristic_category', admin.RelatedOnlyFieldListFilter),
    )


class CharacteristicAdmin(admin.ModelAdmin):
    list_display = ('name', 'characteristic_category')
    search_fields = ('name',)
    list_filter = (
        ('characteristic_category', admin.RelatedOnlyFieldListFilter),
    )

admin.site.register(TypeCategoryProduct)
admin.site.register(ProductSlide)
admin.site.register(ProductCategory)

admin.site.register(Product, ProductAdmin)
admin.site.register(YatchTypeCharacteristicCategory)
admin.site.register(YatchCharacteristicCategory,
                    YatchCharacteristicCategoryAdmin)
admin.site.register(YatchCharacteristic, CharacteristicAdmin)
admin.site.register(SlideShow, SlideShowAdmin)
admin.site.register(Slide)
# admin.site.register(ThreesixtySlider, ItemShowTabAdmin)
admin.site.register(ThreesixtySlider, ItemShowTabAdmin)
admin.site.register(Item)
admin.site.register(Color, ColorAdmin)
admin.site.register(ColorCategory, ColorCategoryAdmin)
admin.site.register(ColorTypeCategory)
admin.site.register(Yatch, YatchAdmin)
