
from django.shortcuts import render, redirect
from django.views.generic.detail import DetailView
from django.views.generic import ListView, View
from .models import *
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from django.core.urlresolvers import reverse
from json import dumps, loads
import random
import os
from io import BytesIO
import time
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm, letter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import Paragraph, Table, TableStyle, Image, Spacer, SimpleDocTemplate, PageBreak, BaseDocTemplate, Frame, PageTemplate
from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY, TA_RIGHT
from reportlab.lib import colors
from reportlab.graphics.shapes import Drawing, Line
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch


import string

from forms import ContactForm
from django.http import HttpResponseRedirect, HttpResponse
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.views.generic.edit import FormView
from django.views.generic.edit import FormMixin


import string


def contacto(request):
    form = ContactForm
    mensaje_mail = None
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.send_email()
            # datos_contacto = Solicitude(nombre_contacto=nombre,correo_electronico=correo,num_telefono=telefono,empresa=empresa,asunto=asunto,mensaje=comentarios)
            # datos_contacto.save()
            mensaje_mail = 'success'

    return render(request, 'contacto.html', {'form': form, 'mensaje_mail': mensaje_mail})


class ContactView(FormMixin, View):
    form_class = ContactForm

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if request.is_ajax():
            if form.is_valid():
                form.send_email()
                return JsonResponse({"message": "Mensaje enviado"})
            else:
                return JsonResponse(form.errors, status=400)
        else:
            return HttpResponseRedirect(reverse('home'))


def pdf(request):

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=myYacht.pdf'

    # http://127.0.0.1:8000/generate_pdf/?yacht_id=5&options=true&characteristic_id=10,11
    yacht_id = int(request.GET.get('yacht_id', ''))
    options = str(request.GET.get('options', 'False'))
    characteristics = str(request.GET.get('characteristic_id', ''))
    characteristics_ids = string.split(characteristics, ',')
    # code = request.session["summary"]["code"]

    yacht = Yatch.objects.get(pk=yacht_id)
    buffer = BytesIO()
    from os.path import abspath, dirname, join, normpath

    BASE_DIR = dirname(dirname(abspath(__file__)))

    Story = []
    logo = normpath(join(BASE_DIR, 'static/images/home/logo-vandutchx2.png'))
    formatted_time = time.ctime()
    im = Image(logo, 3.6 * cm, 0.8 * cm)
    Story.append(im)
    Story.append(Spacer(1, 120))

    # d = Drawing(100, 1)
    # d.add(Line(0, 0, 450, 0))
    # Story.append(d)

    styles = getSampleStyleSheet()
    Story.append(Spacer(1, 40))
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    styles.add(ParagraphStyle(name='Right', alignment=TA_RIGHT))

    ptext = '<font size=12>%s</font>' % formatted_time
    Story.append(Paragraph(ptext, styles["Right"]))
    Story.append(Spacer(1, 12))
    Story.append(Spacer(1, 12))
    ptext = '<font size=12>Dear Sir, dear Madam,</font>'
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 12))
    ptext = '<font size=12>Thank you for your interest in VanDutch and for configuring your Yacht in the VanDutch Yacht Configurator.</font>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 12))
    ptext = '<font size=12>The VanDutch brand is the embodiment of sporty driving. Our Yachts represent uncompromising innovation combined with the typical Yacht characteristics of technology, design, agility and safety which merge harmoniously in each model.</font>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 12))
    ptext = '<font size=12>We trust that you were able to configure your ideal Yacht. For the way you live. For the way you drive. We invite you to take the next logical step with a test drive at your local VanDutch dealer.</font>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 12))
    ptext = '<font size=12>Additional detailed information is available at your local authorized VanDutch dealership, where they will be able to provide you assistance with financing, leasing, and accessories.</font>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 12))
    ptext = '<font size=12>You can also save your configuration or send it directly to your preferred VanDutch Center.</font>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 12))
    ptext = '<font size=12>You will be able to find further information and locations of your local VanDutch dealership at www.vandutch.com</font>'
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 12))
    Story.append(Spacer(1, 12))
    Story.append(Spacer(1, 12))

    ptext = '<font size=12>Your VanDutch Internet Team.</font>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 40))
    Story.append(PageBreak())

    # second page
    d = Drawing(400, 1)
    d.add(Line(0, 0, 450, 0))
    Story.append(d)
    Story.append(Spacer(1, 40))
    ptext = '<strong><font size=20>' + str(yacht.name) + '</font></strong>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 50))

    # end slideshow

    slideshow = SlideShow.objects.get(
        pk=request.session['summary'][yacht]["slideshow_initial"])
    slides = Slide.objects.filter(slideShow=slideshow).order_by('order')
    for slide in slides:
        ruta_img = 'media/' + str(slide.image)
        img_slide = normpath(join(BASE_DIR, ruta_img))
        im = Image(img_slide, 14 * cm, 8 * cm)
        Story.append(im)
        Story.append(Spacer(1, 20))
    ptext = '<strong><font size=10>Important Information</font></strong>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 12))
    ptext = '<font size=10>Although this image is intended to reflect your actual yacht configuration, there may be some variation between this picture and the actual yacht. Some items shown are European specifications.</font>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 20))
    Story.append(PageBreak())

    # characteritics
    for c in characteristics_ids:
        # specifications = []
        typeCategory = YatchTypeCharacteristicCategory.objects.get(pk=int(c))
        categories = YatchCharacteristicCategory.objects.filter(
            yatchcharacteristic__yatch=yacht_id, type_characteristic_category=typeCategory).distinct()
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
        ptext = '<strong><font size=12>' + \
            str(typeCategory.name) + '</font></strong>'
        Story.append(Paragraph(ptext, styles["Justify"]))
        d = Drawing(100, 20)
        d.add(Line(0, 0, 450, 0))
        Story.append(d)
        Story.append(Spacer(1, 12))

        for category in categories:
            # dict_aux_cat = {'attr': category.name, 'val': ' '}
            # specifications.append(dict_aux_cat)
            ptext = '<strong><font size=10>' + \
                str(category.name) + '</font></strong>'
            Story.append(Paragraph(ptext, styles["Justify"]))
            Story.append(Spacer(1, 12))

            characteristics = YatchCharacteristic.objects.filter(
                yatch=yacht_id, characteristic_category=category).distinct()
            for characteristic in characteristics:
                ptext = '<font size=10>' + \
                    str(characteristic.name) + ' ' + \
                    characteristic.value + '</font>'
                Story.append(
                    Paragraph(ptext, styles["Justify"], bulletText='*'))

            d = Drawing(100, 5)
            d.add(Line(0, 0, 450, 0))
            Story.append(d)
            Story.append(Spacer(1, 12))

        Story.append(PageBreak())

        # print specifications
        # styles = getSampleStyleSheet()
        # styleBH = styles['Normal']
        # styleBH.alignment = TA_CENTER
        # styleBH.fontSize = 18

        # caracteristica = Paragraph(typeCategory.name, styleBH)

        # valor = ""

        # data = []
        # data.append([caracteristica, valor])

        # styleN = styles['BodyText']
        # styleN.alignment = TA_CENTER
        # styleN.fontSize = 12

        # for specification in specifications:
        #     spec = [specification['attr'], specification['val']]
        #     data.append(spec)
        #     # high = high - 18
        # # table size
        # width, height = A4
        # table = Table(data, colWidths=[8.5 * cm, 8.5 * cm])

        # table_style = TableStyle(
        #     [
        #         # ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        #         # ('BOX',(0,0),(-1,-1), 0.25, colors.black)
        #     ])
        # table.setStyle(table_style)
        # Story.append(table)
        # Story.append(PageBreak())

    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))

    Story.append(Spacer(1, 50))
    ptext = '<strong><font size=12>My configuration</font></strong>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 8))
    d = Drawing(100, 5)
    d.add(Line(0, 0, 450, 0))
    Story.append(d)

    datos = [
        ('Order No.', 'Yacht', 'Prices*'),
        ('991120', yacht.name, '$ 0'),
    ]

    table3 = Table(datos,   colWidths=[5.5 * cm, 7.5 * cm, 2 * cm])
    table_style = TableStyle([
        # ('TEXTCOLOR',(0,0),(-1,-2),colors.red),

    ])
    table3.setStyle(table_style)
    Story.append(table3)

    if options == "true":
        Story.append(Spacer(1, 50))
        ptext = '<strong><font size=12>Individualisation</font></strong>'
        Story.append(Paragraph(ptext, styles["Justify"]))
        Story.append(Spacer(1, 8))
        d = Drawing(100, 5)
        d.add(Line(0, 0, 450, 0))
        Story.append(d)
        Story.append(Spacer(1, 8))
        products = []
        for p in request.session["summary"][yacht]["options"].iteritems():
            product = Product.objects.get(pk=int(p[1]["pk"]))
            dict_aux_cat = {'category': product.category.product_type_category,
                            'name': product.name, 'price': '$ ' + str(0)}
            products.append(dict_aux_cat)
        datos = [
            ('Category', 'Individual equipment', 'Prices*')
        ]
        for product in products:
            produc = [product['category'], product['name'], '$ 0']
            datos.append(produc)

            # table size
        table2 = Table(datos,  colWidths=[5.5 * cm, 7.5 * cm, 2 * cm])
        table_style = TableStyle([
        ])
        table2.setStyle(table_style)
        Story.append(table2)

    Story.append(Spacer(1, 8))
    d = Drawing(100, 5)
    d.add(Line(0, 0, 450, 0))
    Story.append(d)

    # Story.append(Spacer(1, 50))
    # ptext = '<font size=12>Yacht price</font>'
    # Story.append(Paragraph(ptext, styles["Justify"]))
    # Story.append(Spacer(1, 8))
    # d = Drawing(100, 5)
    # d.add(Line(0, 0, 450, 0))
    # Story.append(d)

    ptext = '<font size=8>* All information is subject to change accept liability arising from the use of any information contained herein. Only an actual invoice issued by PCNA at the time a yacht is sold to an authorized VanDutch dealer may be used as an official indication of equipment and pricing. The Total Manufacturers Suggested Retail Price (MSRP) shown excludes taxes, title, registration, other optional or regionally required equipment, and dealer charges. The price and availability of Individually Commissioned Equipment (CXX) can be determined only after review and analysis by the manufacturer. Actual selling prices are set by dealers and may vary.</font>'
    Story.append(Paragraph(ptext, styles["Justify"]))
    Story.append(Spacer(1, 8))
    d = Drawing(100, 5)
    d.add(Line(0, 0, 450, 0))
    Story.append(d)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=myVanDutch-' + \
        yacht.name + '.pdf'

    styles = getSampleStyleSheet()
    styleN = styles['Normal']
    # styleH = styles['Heading1']
    styleH = getSampleStyleSheet()
    styleH.add(ParagraphStyle(name='center', alignment=TA_CENTER))

    PAGE_HEIGHT = defaultPageSize[1]
    PAGE_WIDTH = defaultPageSize[0]
    Title = "Hello world"

    def footer(canvas, doc):
        canvas.saveState()
        P = Paragraph(
            "This is a multi-line footer.  It goes on every page.  " * 5, styleN)
        w, h = P.wrap(doc.width, doc.bottomMargin)
        P.drawOn(canvas, doc.leftMargin, h)
        canvas.restoreState()

    def myFirstPage(canvas, doc):
        canvas.saveState()
        # canvas.setFont('Times-Bold',16)
        # canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-108, Title)
        canvas.setFont('Times-Roman', 9)
        canvas.drawCentredString(inch, 0.75 * inch, "Page 1")
        canvas.restoreState()

    def myLaterPages(canvas, doc):
        canvas.saveState()
        canvas.setFont('Times-Roman', 9)
        canvas.drawCentredString(
            PAGE_WIDTH / 2.0, PAGE_HEIGHT - 800, "Page %d" % doc.page)
        canvas.restoreState()

    doc = SimpleDocTemplate(buffer, pagesize=letter, rightMargin=72,
                            leftMargin=72, topMargin=32, bottomMargin=18)
    # doc = BaseDocTemplate(buffer, pagesize=letter)
    # frame = Frame(doc.leftMargin, doc.bottomMargin, doc.width, doc.height, id='normal')
    # template = PageTemplate(id='test', frames=frame, onPage=footer)
    # doc.addPageTemplates([template])

    doc.build(Story, onFirstPage=myFirstPage, onLaterPages=myLaterPages)

    # doc.build(Story)
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response


def get_code(code):
    if code == '':
        code = ''.join(random.choice(
            '0123456778910ABCDEFGHIJKLMNOPQRSTUVXYZ') for i in range(8))
    obj = CustomConfig.objects.filter(code=code)

    if obj:
        return get_code('')
    else:
        return code


def generate_code(request):
    mimetype = 'application/json'
    codigo = None
    if request.is_ajax():
        yacht = request.POST.get('yacht_id', '')
        try:
            print 'Try generate code'
            # del self.request.session['summary']
            if request.session['summary'][yacht]["codigo"]:
                codigo = request.session['summary'][yacht]["codigo"]
            # code = get_code('')

        except Exception, e:
            print 'Exception generate code'
            json = dumps(request.session['summary'][yacht])
            code = get_code('')
            CustomConfig.objects.create(code=code, configuration=json)
            request.session.modified = True
            request.session['summary'][yacht]["codigo"] = code
            codigo = code
            success = codigo
        else:
            success = codigo
    else:
        success = 'fail'
    return HttpResponse(dumps(success), mimetype)


def get_color_details(request):
    mimetype = 'application/json'
    color = None
    data = None
    fail_color = None
    if request.is_ajax():
        color_id = request.POST.get('color_id', '')
        try:
            color_object = Color.objects.get(pk=int(color_id))
            color = serializers.serialize('python', [color_object])
        except Exception, e:
            fail_color = 'except color'
        else:
            pass

        data = {'fail_color': fail_color, 'color': color}
        return HttpResponse(dumps(data), mimetype)
    else:
        data = 'fail2'
    return HttpResponse(dumps(data), mimetype)


def get_product_details(request):
    mimetype = 'application/json'
    product = None
    slides = None
    data = None
    fail_slides = None
    fail_product = None
    if request.is_ajax():
        product_id = request.POST.get('product_id', '')
        try:
            # product = {}
            product_object = Product.objects.get(pk=int(product_id))
            product = serializers.serialize('python', [product_object])

            # product["name"] = product_object.name
            # product["shortDescription"] = product_object.shortDescription
            # product["description"] = product_object.description
            # product["price"] = product_object.price
            # product["category"] = product_object.category.name
            # product["product_type_category"] = product_object.category.product_type_category.name
            # product["image"] = product_object.image.url
            # product["image_thumbnail"] = product_object.image_thumbnail.url

        except Exception, e:
            fail_product = 'except product'
        else:
            pass
        try:
            slides_product = ProductSlide.objects.filter(
                product=product_object).order_by('order').values('image')
            slides = []
            for slide in slides_product:
                slides.append(slide["image"])
            data = slides
        except Exception, e:
            fail_slides = 'except product slides'
        else:
            pass
        data = {'fail_product': fail_product, 'fail_slides': fail_slides,
                'product': product, 'product_slides': slides}
        return HttpResponse(dumps(data), mimetype)
    else:
        data = 'fail2'
    return HttpResponse(dumps(data), mimetype)


def get_slides_360(request):
    mimetype = 'application/json'
    if request.is_ajax():
        yacht_id = request.POST.get('yacht_id', '')
        ids_colores = []
        for category in request.session['summary'][yacht]["categories_selected"].iteritems():
            typecategoryobject = ColorTypeCategory.objects.get(pk=category[0])
            typecategoryaux = {}
            typecategoryaux["name"] = typecategoryobject.name
            color = category[1][1]
            typecategoryaux["color"] = color
            request.session.modified = True
            ids_colores.append(color["pk"])
        try:
            slideshow_360 = ThreesixtySlider.objects.filter(
                yatch=yacht_id, color__pk=ids_colores[0]).filter(color__pk=ids_colores[1])
            slides_360 = Item.objects.filter(
                threesixty_slider=slideshow_360[0]).values('image')
            slides = []
            for slide in slides_360:
                slides.append(slide["image"])
            data = slides
        except Exception, e:
            data = 'fail'
        else:
            return HttpResponse(dumps(data), mimetype)
    else:
        data = 'fail2'

    return HttpResponse(data, mimetype)


def update_summary(request):
    mimetype = 'application/json'

    if request.is_ajax():
        yacht = request.POST.get('yacht_id', '')
        print yacht
        summary_type = request.POST.get('summary_type', '')
        cmd = request.POST.get('cmd', '')
        if 'codigo' in request.session['summary'][yacht]:
            request.session.modified = True
            del request.session["summary"][yacht]["codigo"]

        if summary_type == 'options':
            success = 'fail'
            product_id = request.POST.get('product_id', '')
            productobject = Product.objects.get(pk=int(product_id))
            product_qty = request.POST.get('product_qty', '')
            if cmd == 'add':
                product_aux = {}
                product_aux["pk"] = product_id
                product_aux["qty"] = product_qty
                product_aux["name"] = productobject.name
                # product_aux["image"] = productobject.image.url
                # Agregar mas campos de productobject
                request.session.modified = True
                request.session['summary'][yacht]["options"][str(product_id)] = {}
                request.session['summary'][yacht]["options"][
                    str(product_id)] = product_aux
                print request.session['summary'][yacht]["options"]
                # print request.session['summary']["options"]
                success = 'success'
            elif cmd == 'update':
                try:
                    request.session.modified = True
                    request.session['summary'][yacht]["options"][
                        str(product_id)]["qty"] = product_qty
                except Exception, e:
                    product_aux = {}
                    product_aux["pk"] = product_id
                    product_aux["qty"] = product_qty
                    request.session.modified = True
                    request.session['summary'][yacht]["options"][str(product_id)] = {}
                    request.session['summary'][yacht]["options"][
                        str(product_id)] = product_aux
                else:
                    pass
                success = 'success'
            elif cmd == 'delete':
                request.session.modified = True
                del request.session['summary'][yacht]["options"][str(product_id)]
                print request.session['summary'][yacht]["options"]
                success = 'success'

            data = dumps(success)
            return HttpResponse(data, mimetype)
        elif summary_type == 'categories_selected':
            color_id = request.POST.get('color_id', '')
            typeCategory = request.POST.get('typeCategory', '')
            category = request.POST.get('category', '')
            ids_colores = []

            try:
                request.session.modified = True
                del request.session['summary'][yacht][
                    "categories_selected"][str(typeCategory)]
                colorobject = Color.objects.get(pk=int(color_id))
                coloraux = {}
                coloraux['pk'] = colorobject.pk
                coloraux['name'] = colorobject.name
                coloraux['code'] = colorobject.code
                coloraux['price'] = colorobject.price
                # Agregar mas campos de productobject
                request.session['summary'][yacht]["categories_selected"][
                    str(typeCategory)] = (int(category), coloraux)
                print "Try update summary"
                print request.session['summary'][yacht]
            except Exception, e:
                request.session.modified = True

                request.session['summary'][yacht][
                    "categories_selected"][str(typeCategory)] = {}
                colorobject = Color.objects.get(pk=int(color_id))
                coloraux = {}
                coloraux['pk'] = colorobject.pk
                coloraux['name'] = colorobject.name
                coloraux['code'] = colorobject.code
                coloraux['price'] = colorobject.price
                # Agregar mas campos de productobject
                request.session.modified = True
                request.session['summary'][yacht]["categories_selected"][
                    str(typeCategory)] = (int(category), coloraux)
                print "except update summary"

            else:
                pass
            request.session['summary'][yacht]["categories"] = []

            for category in request.session['summary'][yacht]["categories_selected"].iteritems():
                typecategoryobject = ColorTypeCategory.objects.get(pk=category[
                                                                   0])
                typecategoryaux = {}
                typecategoryaux["name"] = typecategoryobject.name
                color = category[1][1]
                typecategoryaux["color"] = color
                request.session.modified = True
                request.session['summary'][yacht][
                    "categories"].append(typecategoryaux)
                ids_colores.append(color["pk"])
            data_slides = None
            data_slides_360 = None
            data_fail_360 = None
            data_fail = None
            slides = None
            slides_360 = None
            try:
                slideshow = SlideShow.objects.filter(Yatch=yacht, color__pk=ids_colores[
                                                     0]).filter(color__pk=ids_colores[1])
                request.session.modified = True
                request.session['summary'][yacht][
                    "slideshow_initial"] = slideshow[0].pk
                slides = Slide.objects.filter(
                    slideShow=slideshow[0]).order_by('order').values()

                slides = list(slides)

            except Exception, e:
                request.session.modified = True
                request.session['summary'][yacht]["slideshow_initial"] = None
                data_fail = 'fail'
            else:
                pass
                # data_slides = serializers.serialize('json', list(slides), fields=('image','id','name'))
            try:
                slideshow_360 = ThreesixtySlider.objects.filter(
                    yatch=yacht, color__pk=ids_colores[0]).filter(color__pk=ids_colores[1])
                request.session['summary'][yacht][
                    "slideshow_360"] = slideshow_360[0].pk
                slides_360 = Item.objects.filter(
                    threesixty_slider=slideshow_360[0]).values()
                slides_360 = list(slides_360)
            except Exception, e:
                request.session.modified = True
                request.session['summary'][yacht]["slideshow_360"] = None
                data_fail_360 = 'fail'
            else:
                pass
            print request.session['summary'][yacht]["categories_selected"]

            # data_slides_360 = serializers.serialize('json', list(slides_360), fields=('image','id','name'))
            data = {'data_fail': data_fail, 'data_fail_360': data_fail_360,
                    'data_slides': slides, 'data_slides_360': slides_360}
            return HttpResponse(dumps(data), mimetype)
    else:
        data = 'fail'
    return HttpResponse(data, mimetype)


class HomeView(ListView):
    model = Yatch
    queryset = Yatch.objects.order_by('order')
    context_object_name = 'yatchs'

    def get_template_names(self):
        return 'home.html'


class YatchDetailView(DetailView):
    model = Yatch
    context_object_name = 'single_yatch'
    form_class = ContactForm
    success_message = None

    # Sacar los parametros de base de datos o sesion.
    def get_context_data(self, **kwargs):

        context = super(YatchDetailView, self).get_context_data(**kwargs)
        context['types'] = ColorTypeCategory.objects.filter(
            colorcategory__color__yatch=self.kwargs['pk']).order_by('name').distinct()
        context['categories'] = {}
        categorias_iniciales = {}
        options = None

        # Tipo de categorias de colores
        for typeCategory in context['types']:
            categories = ColorCategory.objects.filter(color__yatch=self.kwargs[
                                                      'pk'], color_type_category=typeCategory).order_by('name').distinct()
            context['categories'][typeCategory.name] = []
            # print context['categories']
            categorias_iniciales[typeCategory.pk] = {}
            count = 0
            for category in categories:
                colors = Color.objects.filter(
                    yatch=self.kwargs['pk'], color_category=category).order_by('id').distinct()
                context['categories'][typeCategory.name].append(
                    (category.name, colors))
            if count == 0:
                colorobject = colors[:1]
                coloraux = {}
                coloraux['pk'] = colorobject[0].pk
                coloraux['name'] = colorobject[0].name
                coloraux['code'] = colorobject[0].code
                coloraux['price'] = colorobject[0].price
                categorias_iniciales[typeCategory.pk] = (
                    int(category.pk), coloraux)
                count = count + 1

        # Tipo de categorias de productos
        context['products-types'] = TypeCategoryProduct.objects.filter(
            productcategory__product__yatch=self.kwargs['pk']).distinct()
        context['products_categories'] = {}
        # show the products with categories
        for typeCategory in context['products-types']:
            categories = ProductCategory.objects.filter(
                product__yatch=self.kwargs['pk'], product_type_category=typeCategory).distinct()
            context['products_categories'][typeCategory.name] = []
            for category in categories:
                products = Product.objects.filter(
                    yatch=self.kwargs['pk'], category=category).distinct()
                context['products_categories'][
                    typeCategory.name].append((category.name, products))

        # show the characteristic   with categories
        context['characteristic_types'] = YatchTypeCharacteristicCategory.objects.filter(
            yatchcharacteristiccategory__yatchcharacteristic__yatch=self.kwargs['pk']).order_by('name').distinct()
        context['yate_characteristic'] = {}
        for typeCategory in context['characteristic_types']:
            categories = YatchCharacteristicCategory.objects.filter(yatchcharacteristic__yatch=self.kwargs[
                                                                    'pk'], type_characteristic_category=typeCategory).distinct()
            context['yate_characteristic'][typeCategory.name] = []
            for category in categories:
                characteristics = YatchCharacteristic.objects.filter(
                    yatch=self.kwargs['pk'], characteristic_category=category).distinct()
                context['yate_characteristic'][typeCategory.name].append(
                    (category.name, characteristics))

        context['yate_characteristic'] = context[
            'yate_characteristic'].iteritems()
        context['products_categories'] = context[
            'products_categories'].iteritems()
        context['categories'] = sorted(context['categories'].iteritems())
        yacht = self.kwargs['pk']
        if 'summary' not in self.request.session:
            self.request.session['summary'] = {}

        if yacht not in self.request.session['summary']:
            self.request.session['summary'][yacht] = {}

        # print self.request.session["summary"][yacht]["options"]
        self.request.session['summary'][yacht]["yacht"] = self.kwargs['pk']
        self.request.session['summary']["yacht"] = self.kwargs['pk']
        self.request.session['summary'][yacht]["categories"] = []
        ids_colores_iniciales = []

        if self.request.session["summary"][yacht].get('codigo'):

            code = self.request.session["summary"][yacht]["codigo"]
            try:
                customconfig = CustomConfig.objects.filter(code=code)
                confs = loads(customconfig[0].configuration)

            except Exception, e:
                pass

            else:
                products = []
                for p in confs["options"].iteritems():
                    # Agregar Try catch
                    product = Product.objects.get(pk=int(p[1]["pk"]))
                    products.append(product)

                if confs['slideshow_initial'] is None:
                    slideshow = None
                else:
                    slideshow = SlideShow.objects.get(
                        pk=int(confs['slideshow_initial']))
                slideshow_inicial = [slideshow]
                categories_selected = confs["categories"]
                options = confs["options"]
                if confs.get('slideshow_360') is None:
                    slideshow_360 = None
                else:
                    slideshow_360 = ThreesixtySlider.objects.get(
                        pk=int(confs['slideshow_360']))
                slideshow_360 = [slideshow_360]
                categories_selected = confs["categories_selected"]
        else:

            # Categorias iniciales con colores por default
            for category in categorias_iniciales.iteritems():
                typecategoryobject = ColorTypeCategory.objects.get(pk=category[
                                                                   0])
                typecategoryaux = {}
                typecategoryaux["name"] = typecategoryobject.name

                color = category[1][1]
                typecategoryaux["color"] = color
                ids_colores_iniciales.append(color["pk"])
                self.request.session.modified = True
                self.request.session['summary'][yacht][
                    "categories"].append(typecategoryaux)

            # del self.request.session['summary']
            self.request.session.modified = True
            print self.request.session['summary']
            try:
                if self.request.session['summary'][yacht]["categories_selected"]:
                    categories_selected = self.request.session[
                        'summary'][yacht]["categories_selected"]
                    print "Try"
            except Exception, e:

                self.request.session['summary'][yacht][
                    "categories_selected"] = categorias_iniciales  #
                categories_selected = categorias_iniciales
            else:
                pass

            # print categories_selected
            if self.request.GET.get('colors_ids'):
                if 'slideshow_initial' in self.request.session["summary"][yacht]:
                    del self.request.session['summary'][yacht]["slideshow_initial"]

                colors_ids = str(self.request.GET.get('colors_ids', ''))
                colors_ids = string.split(colors_ids, ',')
                print "categories_selected---------------------------------------->"
                print self.request.session["summary"][yacht]["categories_selected"]
                del self.request.session['summary'][yacht]["categories_selected"]
                self.request.session['summary'][yacht]["categories_selected"] = {}
                for color_id in colors_ids:
                    try:
                        colorobject = Color.objects.get(pk=int(color_id))
                        color_category = colorobject.color_category
                        colortypecategory = color_category.color_type_category
                        coloraux = {}
                        coloraux['pk'] = colorobject.pk
                        coloraux['name'] = colorobject.name
                        coloraux['code'] = colorobject.code
                        coloraux['price'] = colorobject.price
                        self.request.session.modified = True
                        self.request.session['summary'][yacht]["categories_selected"][
                            str(colortypecategory.pk)] = (int(color_category.pk), coloraux)

                    except Exception, e:
                        pass
                    else:
                        pass
                categories_selected = self.request.session['summary'][yacht]["categories_selected"]
            self.request.session['summary'][yacht]["categories"] = []
            for category in self.request.session['summary'][yacht]["categories_selected"].iteritems():
                typecategoryobject = ColorTypeCategory.objects.get(pk=category[
                                                                   0])
                typecategoryaux = {}
                typecategoryaux["name"] = typecategoryobject.name
                color = category[1][1]
                typecategoryaux["color"] = color
                self.request.session.modified = True
                self.request.session['summary'][yacht][
                    "categories"].append(typecategoryaux)

            if self.request.session['summary'][yacht].get("categories_selected"):
                self.request.session.modified = True
                self.request.session['summary'][yacht]["categories"] = []
                ids_colores_iniciales = []
                for category in self.request.session['summary'][yacht]["categories_selected"].iteritems():
                    typecategoryobject = ColorTypeCategory.objects.get(pk=category[
                                                                       0])
                    typecategoryaux = {}
                    typecategoryaux["name"] = typecategoryobject.name
                    color = category[1][1]
                    typecategoryaux["color"] = color
                    self.request.session.modified = True
                    self.request.session['summary'][yacht][
                        "categories"].append(typecategoryaux)
                    ids_colores_iniciales.append(color["pk"])

            slideshow_inicial = SlideShow.objects.filter(Yatch=self.kwargs[
                                                         'pk'], color__pk=ids_colores_iniciales[0]).filter(color__pk=ids_colores_iniciales[1])
            try:
                if 'slideshow_initial' in self.request.session["summary"][yacht]:
                    slideshow_inicial = SlideShow.objects.get(
                        pk=self.request.session['summary'][yacht]["slideshow_initial"])
                    slideshow_inicial = [slideshow_inicial]
                    print slideshow_inicial
            except Exception, e:
                pass
            else:
                pass
            if not slideshow_inicial:
                self.request.session.modified = True
                self.request.session['summary'][yacht]["slideshow_initial"] = None
            else:
                self.request.session.modified = True
                self.request.session['summary'][yacht]["slideshow_initial"] = slideshow_inicial[0].pk

            if self.request.session["summary"][yacht].get("options"):
                options = self.request.session['summary'][yacht]["options"]
                # print self.request.session["summary"][yacht]["options"]
            else:
                self.request.session['summary'][yacht]["options"] = {}

            slideshow_360 = ThreesixtySlider.objects.filter(yatch=self.kwargs[
                                                            'pk'], color__pk=ids_colores_iniciales[0]).filter(color__pk=ids_colores_iniciales[1])
            if not slideshow_360:
                self.request.session['summary'][yacht]["slideshow_360"] = None
            else:
                self.request.session['summary'][yacht][
                    "slideshow_360"] = slideshow_360[0].pk

        context['first_slideshow'] = slideshow_inicial
        context['slides_slideshow'] = Slide.objects.filter(
            slideShow=slideshow_inicial[0]).order_by('order')
        context['slideshow_360'] = slideshow_360
        context['options'] = options


        # print categories_selected
        context['categories_selected'] = categories_selected
        print "categoriaaaas"
        print categories_selected

        return context

    def get_template_names(self):
        return 'yatch_detail.html'


def Summary(request, pk):
    entrar = True
    data = None
    codigo = None
    yacht = pk
    print yacht
    yacht2 = Yatch.objects.get(pk=yacht)
    # Codigo
    if request.GET.get('code', ''):
        # print 'get code url'
        request.session.modified = True

        code = request.GET['code']
        if 'summary' not in request.session:
            request.session["summary"] = {}
        try:
            # print 'try url'
            customconfig = CustomConfig.objects.filter(code=code)
            confs = loads(customconfig[0].configuration)
            yacht = confs["yacht"]

            if yacht not in request.session['summary']:
                request.session['summary'][yacht] = {}

            if 'codigo' in request.session["summary"][yacht]:
                del request.session["summary"][yacht]["codigo"]                

            request.session['summary'][yacht] = confs
            # print request.session['summary']
            # print confs
            if 'codigo' not in request.session["summary"][yacht]:
                request.session.modified = True
                request.session["summary"][yacht]["codigo"] = code

        except Exception, e:
            # print 'except url'
            pass
        else:
            # print 'else url'
            products = []
            entrar = False
            # yacht = int(confs['yacht'])

            yacht = Yatch.objects.get(pk=yacht)
            for p in confs["options"].iteritems():
                product = Product.objects.get(pk=int(p[1]["pk"]))
                products.append(product)

            if confs['slideshow_initial'] is None:
                slideshow = None
            else:
                slideshow = SlideShow.objects.get(
                    pk=int(confs['slideshow_initial']))

            categories_selected = confs["categories"]
            options = confs["options"]

            characteristic_types = YatchTypeCharacteristicCategory.objects.filter(
                yatchcharacteristiccategory__yatchcharacteristic__yatch=yacht).order_by('name').distinct()
            yate_characteristic = {}
            for typeCategory in characteristic_types:
                categories = YatchCharacteristicCategory.objects.filter(
                    yatchcharacteristic__yatch=yacht, type_characteristic_category=typeCategory).distinct()
                yate_characteristic[typeCategory.name] = []
                for category in categories:
                    characteristics = YatchCharacteristic.objects.filter(
                        yatch=yacht, characteristic_category=category).distinct()
                    yate_characteristic[typeCategory.name].append(
                        (category.name, characteristics))
            yate_characteristics = yate_characteristic.iteritems()

    # Session

    if 'summary' in request.session and entrar:
        products = []
        #yacht_id = int(request.session['summary'][yacht]['yacht'])
        # show the characteristic   with categories
        characteristic_types = YatchTypeCharacteristicCategory.objects.filter(
            yatchcharacteristiccategory__yatchcharacteristic__yatch=yacht).order_by('name').distinct()
        yate_characteristic = {}
        for typeCategory in characteristic_types:
            categories = YatchCharacteristicCategory.objects.filter(
                yatchcharacteristic__yatch=yacht, type_characteristic_category=typeCategory).distinct()
            yate_characteristic[typeCategory.name] = []
            for category in categories:
                characteristics = YatchCharacteristic.objects.filter(
                    yatch=yacht, characteristic_category=category).distinct()
                yate_characteristic[typeCategory.name].append(
                    (category.name, characteristics))
        yate_characteristics = yate_characteristic.iteritems()

        for p in request.session['summary'][yacht]["options"].iteritems():
            product = Product.objects.get(pk=int(p[1]["pk"]))
            products.append(product)
        if request.session['summary'][yacht]["slideshow_initial"] is None:
            return HttpResponseRedirect()
            slideshow = None
        else:
            slideshow = SlideShow.objects.get(
                pk=int(request.session['summary'][yacht]["slideshow_initial"]))
        categories_selected = request.session["summary"][yacht]["categories"]

        # Productos seleccionados cuando se precargo el codigo.
        options = request.session["summary"][yacht]["options"]

    else:
        if 'summary' in request.session and entrar == False:
            pass
        else:
            return redirect('http://punklabs.ninja/devvandutch/')

    slides_slideshow = Slide.objects.filter(
        slideShow=slideshow).order_by('order')

    data = {'slides_slideshow': slides_slideshow, 'options': options, 'categories': categories_selected,
            'yate_characteristic': yate_characteristics, 'slideshow': slideshow, 'single_yatch': yacht2, 'codigo': codigo}

    return render(request, 'summary.html', data)


def code(request, code):
    data = {'code': code}
    return render(request, 'code.html', data)


def blankcode(request):
    return render(request, 'code.html')


def summary_code(request):
    code = request.GET.get('code', '')
    customconfig = CustomConfig.objects.filter(code=code)
    confs = loads(customconfig[0].configuration)
    yacht = confs["yacht"]
    return HttpResponseRedirect(reverse('summary', args=(yacht)))


# Agregar view para ingresar codigo.
# consultamos base de datos si existe continuas si no pues un mensaje de error.

    # enviar  form por GET hacia la url summary
    # guardar en sesion el codigo
