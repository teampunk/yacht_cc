from __future__ import unicode_literals
from sorl.thumbnail import ImageField
from django.db import models
from django.contrib.postgres.fields import JSONField


class CustomConfig(models.Model):
    code = models.CharField(max_length=50,)
    configuration = JSONField()
    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.code


class TypeCategoryProduct(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    # order = models.IntegerField(null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Product Type Category"
        verbose_name_plural = "Product Type Categories"

    def __unicode__(self):
        return self.name


class ProductCategory(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    product_type_category = models.ForeignKey(TypeCategoryProduct, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name = "Product Category"
        verbose_name_plural = "Product Categories"

    def __unicode__(self):
        return '%s - %s' % (self.name, self.product_type_category)


class Product(models.Model):

    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=True, null=True)
    shortDescription = models.CharField(max_length=500, blank=True, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True)
    image = ImageField(upload_to='products', help_text='741 x 317', blank=True, null=True)
    image_thumbnail = ImageField(upload_to='products', help_text='741 x 317', blank=True, null=True)
    price = models.IntegerField(null=True, default=0, blank=True)
    order = models.IntegerField(null=True, blank=True, default=0)

    def __unicode__(self):
        return '%s - %s - %s' % (self.name, self.category, self.category.product_type_category)


class ProductSlide(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=True, null=True)
    image = ImageField(upload_to='products/images', help_text='')
    # imagefull = ImageField(upload_to='slideshow', help_text='741 x 317')
    order = models.IntegerField(null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Product Slide"
        verbose_name_plural = "Product Slides"

    def __unicode__(self):
        return self.name


class ColorTypeCategory(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    # order = models.IntegerField(null=True, blank=True, default=0)

    def __unicode__(self):
        return self.name


class ColorCategory(models.Model):
    color_type_category = models.ForeignKey(ColorTypeCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=True, null=True)
    order = models.IntegerField(null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Color Category"
        verbose_name_plural = "Color Categories"

    def __unicode__(self):
        return self.name


class Color(models.Model):
    color_category = models.ForeignKey(ColorCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=True, null=True)
    code = models.CharField(max_length=100, blank=True, null=True)
    price = models.IntegerField(null=True, default=0, blank=True)
    order = models.IntegerField(null=True, blank=True, default=0)
    image = ImageField(upload_to='products/images', help_text='', blank=True, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True)

    def type(self):
        return self.color_category.color_type_category

    def __unicode__(self):
        return '%s - %s -%s' % (self.name, self.color_category, self.color_category.color_type_category)


class Yatch(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    thumbnail = ImageField(upload_to='yatchs', help_text='741 x 317', blank=True, null=True)
    product = models.ManyToManyField(Product)
    color = models.ManyToManyField(Color)
    order = models.IntegerField(null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Yacht"
        verbose_name_plural = "Yachts"

    def __unicode__(self):
        return self.name


class YatchTypeCharacteristicCategory(models.Model):
    name = models.CharField(max_length=255)
    order = models.IntegerField(null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Yacht Type Characteristic"
        verbose_name_plural = "Yacht Type Characteristics"

    def __unicode__(self):
        return self.name


class YatchCharacteristicCategory(models.Model):
    name = models.CharField(max_length=255)
    type_characteristic_category = models.ForeignKey(YatchTypeCharacteristicCategory, on_delete=models.CASCADE)
    order = models.IntegerField(null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Yacht Characteristic"
        verbose_name_plural = "Yacht Characteristics"

    def __unicode__(self):
        return '%s - %s ' % (self.name, self.type_characteristic_category)


class YatchCharacteristic(models.Model):
    name = models.CharField(max_length=255)
    value = models.CharField(max_length=255, blank=True)
    yatch = models.ForeignKey(Yatch, on_delete=models.CASCADE)
    characteristic_category = models.ForeignKey(YatchCharacteristicCategory, on_delete=models.CASCADE)
    order = models.IntegerField(null=True, blank=True, default=0)

    class Meta:
        verbose_name = "Yacht Characteristic"
        verbose_name_plural = "Yacht Characteristics"

    def __unicode__(self):
        return self.name


class SlideShow(models.Model):
    name = models.CharField(max_length=100)
    Yatch = models.ForeignKey(Yatch, on_delete=models.CASCADE)
    color = models.ManyToManyField(Color)

    def vandutch(self):
        return self.Yatch

    def __unicode__(self):
        return self.name


class Slide(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    slideShow = models.ForeignKey(SlideShow, on_delete=models.CASCADE)
    image = ImageField(upload_to='slideshow', help_text='741 x 317')
    imagefull = ImageField(upload_to='slideshow', help_text='741 x 317', blank=True)
    order = models.IntegerField(null=True, blank=True, default=0)

    def __unicode__(self):
        return self.name


class ThreesixtySlider(models.Model):
    name = models.CharField(max_length=100)
    yatch = models.ForeignKey(Yatch, on_delete=models.CASCADE)
    color = models.ManyToManyField(Color)

    def vandutch(self):
        return self.yatch

    def __unicode__(self):
        return self.name


class Item(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    threesixty_slider = models.ForeignKey(ThreesixtySlider, on_delete=models.CASCADE)
    image = ImageField(upload_to='threesixty')
    order = models.IntegerField(null=True, blank=True, default=0)
    # imagefull = ImageField(upload_to='slideshow', help_text='741 x 317')

    def __unicode__(self):
        return self.name
