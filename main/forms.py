from django import forms
from django.core.mail import EmailMessage
from django.forms import HiddenInput


class ContactForm(forms.Form):
    # salutation = forms.CharField(label="salutation", max_length=10)
    CHOICES = (('-None-', 'Salutation',), ('Mr.', 'Mr.',), ('Mrs.', 'Mrs.',), ('Ms.', 'Ms.',), ('Dr.', 'Dr.',), ('Prof.', 'Prof.',))
    salutation = forms.ChoiceField(widget=forms.Select, choices=CHOICES)
    firstname = forms.CharField(label="First name *", max_length=50)
    lastname = forms.CharField(label="Last Name *", max_length=50)
    correo = forms.EmailField(label="Email *")
    telefono = forms.CharField(label="Phone *", max_length=15)
    boating = forms.CharField(label="Where are you boating *", max_length=100)
    option2 = (('-None-', 'Select an option *',), ('Sales', 'Sales',), ('Test Drive', 'Test Drive',), ('Media/Pres', 'Brochure Request',), ('Media/Pres', 'Media/Press',), ('Service & Maintenance', 'Service & Maintenance',))
    option = forms.ChoiceField(widget=forms.Select, choices=option2)
    comentarios = forms.CharField(label="Inquiry *", widget=forms.Textarea)
    code = forms.CharField(max_length=15, widget=forms.HiddenInput())
    yacht = forms.CharField(max_length=15, widget=forms.HiddenInput())
    cexterior = forms.CharField(max_length=15, widget=forms.HiddenInput())
    cinterior = forms.CharField(max_length=15, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['placeholder'] = self.fields[field].label
            self.fields[field].label = False

    def send_email(self):
        from_email = 'test@vandutch.com'
        to = ['juan.yofu@vandutch.com']
        bcc = ['martin@punkmkt.com']
        salutation = self.cleaned_data["salutation"]
        firstname = self.cleaned_data["firstname"]
        lastname = self.cleaned_data["lastname"]
        email = self.cleaned_data["correo"]
        phone = self.cleaned_data["telefono"]
        boating = self.cleaned_data["boating"]
        option2 = self.cleaned_data["option"]
        comments = self.cleaned_data["comentarios"]
        code = self.cleaned_data["code"]
        yacht = self.cleaned_data["yacht"]
        cexterior = self.cleaned_data["cexterior"]
        cinterior = self.cleaned_data["cinterior"]
        subject = 'You have a new email from Color Configurator (' + yacht + ')'
        email_body = 'You have a new email from Color Configurator (' + salutation + ' ' + firstname + ' ' + lastname + ' ) '
        email_body += '\nFirstname: ' + salutation + firstname
        email_body += '\nLastname: ' + lastname
        email_body += '\nPhone: ' + phone
        email_body += '\nEmail: ' + email
        email_body += '\nWhere are you boating: ' + boating
        email_body += '\nOption: ' + option2
        email_body += '\nInquiry: ' + comments
        email_body += '\nCode: ' + code
        email_body += '\nYacht Model: ' + yacht
        email_body += '\nExterior Color: ' + cexterior
        email_body += '\nInterior Color: ' + cinterior
        email_body += '\nLink: ' + 'http://configurator.vandutch.com/code/' + code
        email_msg = EmailMessage(subject, email_body, from_email, to, bcc)
        return email_msg.send(fail_silently=True)
