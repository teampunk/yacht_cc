jQuery(
function($) {
	
	$(document).ready(function(){
		var contentButton = [];
		var contentTop = [];
		var content = [];
		var lastScrollTop = 160;
		var scrollDir = '';
		var itemClass = '';
		var itemHover = '';
		var menuSize = null;
		var stickyHeight = 0;
		var stickyMarginB = 0;
		var currentMarginT = 0;
		var topMargin = 0;
		var size = $( window ).width();

		$(window).scroll(function(event){
			if (size<768){
				var st = $(this).scrollTop()+160;

			}else if(size >=768 && size <= 1000){
				var st = $(this).scrollTop()+130;

			}else{
				var st = $(this).scrollTop()+160;

			}
   			
   			if (st > lastScrollTop){
       			scrollDir = 'down';
   			} else {
      			scrollDir = 'up';
   			}
  			lastScrollTop = st;
  			var view = $("#navbar").find("li.active").attr('id');
		if(view== 'vint'){
			$('#view-interior').trigger( "click" );
		}else if(view == 'vext'){
			$('#view-aerial').trigger( "click" );
		}

		});

		$.fn.stickUp = function( options ) {
			// adding a class to users div
			$(this).addClass('stuckMenu');
        	//getting options
        	var objn = 0;
        	if(options != null) {
	        	for(var o in options.parts) {
	        		if (options.parts.hasOwnProperty(o)){
	        			content[objn] = options.parts[objn];
	        			objn++;
	        		}
	        	}
	  			if(objn == 0) {
	  				console.log('error:needs arguments');
	  			}

	  			itemClass = options.itemClass;
	  			itemHover = options.itemHover;
	  			if(options.topMargin != null) {
	  				if(options.topMargin == 'auto') {
	  					topMargin = parseInt($('.stuckMenu').css('margin-top'));
	  				} else {
	  					if(isNaN(options.topMargin) && options.topMargin.search("px") > 0){
	  						topMargin = parseInt(options.topMargin.replace("px",""));
	  					} else if(!isNaN(parseInt(options.topMargin))) {
	  						topMargin = parseInt(options.topMargin);
	  					} else {
	  						console.log("incorrect argument, ignored.");
	  						topMargin = 0;
	  					}	
	  				}
	  			} else {
	  				topMargin = 0;
	  			}
	  			menuSize = $('.'+itemClass).size()-1;
  			}			
			stickyHeight = parseInt($(this).height());
			stickyMarginB = parseInt($(this).css('margin-bottom'));
			currentMarginT = parseInt($(this).next().closest('div').css('margin-top'));
			vartop = parseInt($(this).offset().top);
			//$(this).find('*').removeClass(itemHover);
		}
		$(document).on('scroll', function() {

			var st = $(this).scrollTop();
   			
   			if (st > lastScrollTop){
       			varscroll = parseInt($(document).scrollTop()+160);
   			} else {
      			varscroll = parseInt($(document).scrollTop()+160);
   			}
  			lastScrollTop = st;
			
			if(menuSize != null){
				for(var i=0;i <= menuSize-1;i++)
				{
					
					
					var contenido = $('#'+content[i]+'');
					
					// if (contenido.length) {
						// contentTop[i] = contenido.offsetTop;
						contentTop[i] = contenido.offset().top;
					
						function bottomView(i) {
							contentView = contenido.height()*.4;
							testView = contentTop[i] - contentView;

							if(varscroll > testView){
								$('.'+itemClass).removeClass(itemHover);
								$('.'+itemClass+':eq('+i+')').addClass(itemHover);
							} else if(varscroll < 50){
								$('.'+itemClass).removeClass(itemHover);
								$('.'+itemClass+':eq(0)').addClass(itemHover);
							}
						}
						if(scrollDir == 'down' && varscroll > contentTop[i]-50 && varscroll < contentTop[i]+50) {
							$('.'+itemClass).removeClass(itemHover);
							$('.'+itemClass+':eq('+i+')').addClass(itemHover);
						}
						if(scrollDir == 'up') {
							bottomView(i);
						}

					// }
				}
			}



			if(vartop < varscroll + topMargin){
				$('.stuckMenu').addClass('isStuck');
				$('.stuckMenu').next().closest('div').css({
					'margin-top': stickyHeight + stickyMarginB + currentMarginT + 'px'
				}, 10);
				$('.stuckMenu').css("position","fixed");
				$('.isStuck').css({
					top: '0px'
				}, 10, function(){

				});
			};

			if(varscroll + topMargin < vartop){
				$('.stuckMenu').removeClass('isStuck');
				$('.stuckMenu').next().closest('div').css({
					'margin-top': currentMarginT + 'px'
				}, 10);
				$('.stuckMenu').css("position","relative");
			};

		});
	});

});
