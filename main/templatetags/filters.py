from django.template import Library 

register = Library()



@register.filter()
def selected_color(color, array_color):
	# print array_color
	# print color 
	finded = False
	#print array_color
	for category_selected in array_color.iteritems():
		color_selected = category_selected[1][1]["pk"]
		# print "cs: " + str(color_selected) + "c: "+ str(color)
		if color_selected == int(color):
			finded = True
			break
	return finded


@register.filter()
def selected_product(product, array_product):
	finded = False
	if array_product is not None:
		for category in array_product.iteritems():
			product_selected = category[1]['pk']
			if product_selected == str(product):
				finded = True
				break
			
	return finded





